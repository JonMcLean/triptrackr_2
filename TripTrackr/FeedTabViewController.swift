//
//  FeedTabViewController.swift
//  TripTrackr
//
//  Created by Jon McLean on 21/06/2016.
//  Copyright © 2016 TripTrackr. All rights reserved.
//

import UIKit
import SwiftyJSON

class FeedTabViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var ltableView: UITableView!
    
    let network = Networking()
    let storage = SessionStorage()
    
    var posts: [AnyObject] = []
    var currentOffsetCount: Int = 0
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.ltableView.backgroundColor = UIColor(red: 162, green: 162, blue: 162, alpha: 1)
        self.ltableView.tintColor = UIColor(red: 162, green: 162, blue: 162, alpha: 1)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.ltableView.delegate = self
        self.ltableView.dataSource = self
        
        self.ltableView.rowHeight = UITableViewAutomaticDimension;
        self.ltableView.estimatedRowHeight = 454.0; // set to whatever your "average" cell height is
        
        loadFeed(0)
        
        let button = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: Selector("createNew"))
        button.tintColor = UIColor(red: 28, green: 28, blue: 28, alpha: 1) // 28,28,28
        
        self.navigationItem.rightBarButtonItem = button
        
        self.ltableView.registerNib(UINib(nibName: "FeedCell2", bundle: nil), forCellReuseIdentifier: "FeedCell")
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FeedCell", forIndexPath: indexPath) as! FeedCell
        
        let post = self.posts[indexPath.row]
        
        //print(post["posted"] as! String)
        //print((post["first_name"] as! String) + " " + (post["last_name"] as! String))
        
        //print("Main Image: " )
        
        cell.fill((post["first_name"] as! String) + " " + (post["last_name"] as! String), date: post["posted"] as! String, title: post["heading"] as! String, stars: post["rating"] as? String, content: post["content"] as? String, mainImage: post["image"] as? String)
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.row == tableView.indexPathsForVisibleRows!.last!.row) {
            // last element
            //loadFeed(currentOffsetCount + 5)
        }
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func loadFeed(offset: Int) {
        let info = storage.loadSessionInfo()
        
        network.sendURLRequest(urlBase + "/api/feed/\(info["sessionCode"] as! String)/\(offset)") { (data, response, error) -> Void in
            
            if(error != nil) {
                
            }
            
            let code = (response as! NSHTTPURLResponse).statusCode
            
            if(code == 200) {
                let json = JSON(data: data!)
                self.posts = json.arrayObject!
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.ltableView.reloadData()
                    self.currentOffsetCount += offset
                })
                
            }else {
                print("Code: \(code)")
            }
            
        }
    }
    
}
