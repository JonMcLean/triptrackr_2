//
//  FeedCell.swift
//  TripTrackr
//
//  Created by Jon McLean on 22/06/2016.
//  Copyright © 2016 TripTrackr. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {
    
    @IBOutlet var feedCellView: UIView!
    @IBOutlet var author: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var title: UILabel!
    @IBOutlet var mainImage: UIImageView!
    @IBOutlet var stars: UIImageView!
    @IBOutlet var content: UITextView!
    @IBOutlet var stackView: UIStackView!
    
    @IBAction func like(sender: UIButton) {
        // does nothing
    }
    
    @IBAction func share(sender: UIButton) {
        // does nothing
    }
    
    @IBAction func explore(sender: UIButton) {
        // does nothing
    }
    
    func setupCell() {
        
        self.feedCellView.alpha = 1
        self.feedCellView.layer.masksToBounds = false
        self.feedCellView.layer.cornerRadius = 1
        self.feedCellView.layer.shadowOffset = CGSizeMake(-0.2, -0.2)
        self.feedCellView.layer.shadowOpacity = 0.2
        
        //self.feedCellView.bounds = CGRectInset(self.feedCellView.frame, 10, 10)
        
        stackView.layoutMargins = UIEdgeInsets(top: 12, left: 0, bottom: 6, right: 0)
        stackView.layoutMarginsRelativeArrangement = true
    }
    
    // MARK: Fill the cell
    func fill(name: String, date: String, title: String, stars: String?, content: String?, mainImage: String?) {
        
        let mainImageConstraints = self.mainImage.constraints
        let starConstraints = self.stars.constraints
        let contentConstraints = self.content.constraints
        
        self.setupCell()
        
        self.author.text = name
        self.date.text = date
        self.title.text = title
        
        print(mainImage)
        
        if(mainImage == nil) {
            for c in mainImageConstraints {
                if(c.identifier == "mainImageViewConstraint") {
                    self.mainImage.removeConstraint(c)
                }
            }
            
            self.mainImage.frame = CGRectMake(self.mainImage.frame.origin.x, self.mainImage.frame.origin.y, self.mainImage.frame.width, 0)
            self.mainImage.image = nil
        }else {
            self.mainImage.frame = CGRectMake(self.mainImage.frame.origin.x, self.mainImage.frame.origin.y, self.mainImage.frame.width, 138)
            self.mainImage.image = imageFromURL(urlBase + mainImage!)
        }
        
        if(stars == nil) {
            
            for c in starConstraints {
                if(c.identifier == "starImageViewConstraint") {
                    self.stars.removeConstraint(c)
                }
            }
            
            self.stars.frame = CGRectMake(self.stars.frame.origin.x, self.stars.frame.origin.y, self.stars.frame.width, 0)
        }else {
            self.stars.frame = CGRectMake(self.stars.frame.origin.x, self.stars.frame.origin.y, self.stars.frame.width, 40)
            self.stars.image = imageFromURL(urlBase + stars!)
        }
        
        if(content == nil) {
            
            for c in contentConstraints {
                if(c.identifier == "contentConstraint") {
                    self.content.removeConstraint(c)
                }
            }
            self.content.text = ""
        }else {
            self.content.text = content
        }
        
    }
}
