//
//  SessionValidation.swift
//  TripTrackr
//
//  Created by Jon McLean on 21/06/2016.
//  Copyright © 2016 TripTrackr. All rights reserved.
//

import UIKit


class SessionValidation {
    
    // MARK: Need to build validation timer checker thing.
    
    // MARK: Need to test and fix
    func checkValid() {
        print("Checking valid")
        
        let session = SessionStorage()
        let di = session.loadSessionInfo()
        
        let sessionTime: Int = di["expire"] as! Int
        
        if(sessionTime <= Int(NSDate().timeIntervalSince1970)) {
            // expired
            let defaults = SessionStorage()
            defaults.invalidate()
            defaults.clearInformation()
            defaults.isLoggedIn(false)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("LoginNavController") as! UINavigationController
            UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(vc, animated: true, completion: nil)
            
        }else {
            // has not expired
        }
        
    }
}