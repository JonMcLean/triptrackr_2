//
//  LoginSessionController.swift
//  TripTrackr
//
//  Created by Jon McLean on 21/06/2016.
//  Copyright © 2016 TripTrackr. All rights reserved.
//

import Foundation

class SessionStorage: NSObject {
    
    var ud = NSUserDefaults.standardUserDefaults()
    
    func storeSessionInfo(sessionCode: String, lastName: String, firstName: String, id: Int, username: String, expire: Int, banner: String) {
        
        ud.setValue(sessionCode, forKey: "l_sessionCode")
        ud.setValue(lastName, forKey: "l_lastName")
        ud.setValue(firstName, forKey: "l_firstName")
        ud.setValue(id, forKey: "l_id")
        ud.setValue(username, forKey: "l_username")
        ud.setValue(expire, forKey: "l_expire")
        ud.setValue(banner, forKey: "l_banner")
        
        ud.setValue(true, forKey: "l_valid")
        
        ud.synchronize()
    }
    
    func isLoggedIn(boolean: Bool) {
        ud.setBool(boolean, forKey: "isLoggedIn")
    }
    
    func loadSessionInfo() -> Dictionary<String, AnyObject> {
        
        var dictionary: Dictionary<String, AnyObject> = ["":""];
        dictionary.removeAll();
        
        dictionary["sessionCode"] = ud.valueForKey("l_sessionCode") as! String
        dictionary["lastName"] = ud.valueForKey("l_lastName") as! String
        dictionary["firstName"] = ud.valueForKey("l_firstName") as! String
        dictionary["id"] = ud.valueForKey("l_id") as! Int
        dictionary["username"] = ud.valueForKey("l_username") as! String
        dictionary["expire"] = ud.valueForKey("l_expire") as! Int
        dictionary["valid"] = ud.valueForKey("l_valid") as! Bool
        dictionary["banner"] = ud.valueForKey("l_banner") as! String
        
        return dictionary;
    }
    
    func clearInformation() {
        
        ud.removeObjectForKey("l_sessionCode")
        ud.removeObjectForKey("l_lastName")
        ud.removeObjectForKey("l_firstName")
        ud.removeObjectForKey("l_id")
        ud.removeObjectForKey("l_username")
        ud.removeObjectForKey("l_expire")
        ud.removeObjectForKey("l_banner")
        self.isLoggedIn(false)
    }
    
    func invalidate() {
        ud.setValue(false, forKey: "l_valid")
    }
    
}

