//
//  LoadScreenViewController.swift
//  TripTrackr
//
//  Created by Jon McLean on 21/06/2016.
//  Copyright © 2016 TripTrackr. All rights reserved.
//

import UIKit
import CoreLocation

class LoadScreenViewController: UIViewController {
    
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        indicator.startAnimating()
        
        
        self.performSegueWithIdentifier("load_login", sender: self)
        
        
        indicator.stopAnimating()
    }
    
}
