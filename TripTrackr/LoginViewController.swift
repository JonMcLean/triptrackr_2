//
//  LoginViewController.swift
//  TripTrackr
//
//  Created by Jon McLean on 21/06/2016.
//  Copyright © 2016 TripTrackr. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginViewController: UIViewController {
    
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var outcome: UILabel!
    
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        password.secureTextEntry = true
        assignBackgroundImage()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func loginAction(sender: UIButton) {
        
        let network = Networking()
        network.sendPOSTRequest(urlBase + "/api/login", parameters: ["username": username.text!, "password": password.text!]) { (data, response, error) -> Void in
            print("Post Request Sent")
            
            if(error != nil) { // There was an error
                
            }
            
            let code = (response as! NSHTTPURLResponse).statusCode
            print(code)
            
            
            if(code == 200) {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.outcome.textColor = UIColor.greenColor()
                    self.outcome.text = "Success!"
                    
                    let json = JSON(data: data!)
                    let cDic = json.dictionaryObject!
                    
                    let storage = SessionStorage()
                    storage.storeSessionInfo(cDic["session"] as! String, lastName: cDic["last_name"] as! String, firstName: cDic["first_name"] as! String, id: cDic["id"] as! Int, username: cDic["username"] as! String, expire: cDic["expire"] as! Int, banner: cDic["banner"] as! String)
                    
                    
                    print("Hello World")
                    
                    // perform segue
                    self.performSegueWithIdentifier("login_main", sender: self)
                    
                })
            }else if(code == 400){
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.outcome.text = "Incorrect Username or Password! Please try again"
                })
            }else {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.outcome.text = "There was a problem with out servers! Please try again later"
                })
            }
            
        }
        
        
    }
    
    func assignBackgroundImage() {
        let image = UIImage(named: "bgplaceholder")
        var imageView: UIImageView
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode = UIViewContentMode.ScaleAspectFill
        imageView.image = image
        imageView.center = view.center
        imageView.alpha = 0.75
        self.view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    
}
