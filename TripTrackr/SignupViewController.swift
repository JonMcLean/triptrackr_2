//
//  SignupViewController.swift
//  TripTrackr
//
//  Created by Jon McLean on 21/06/2016.
//  Copyright © 2016 TripTrackr. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {
    
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var fName: UITextField!
    @IBOutlet var lName: UITextField!
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.password.secureTextEntry = true
    }
    
    
    
}
