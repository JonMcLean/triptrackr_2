//
//  MainNavigationController.swift
//  TripTrackr
//
//  Created by Jon McLean on 22/06/2016.
//  Copyright © 2016 TripTrackr. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // load image
        let storage = SessionStorage()
        let info = storage.loadSessionInfo()
        let banner = imageFromURL(urlBase + (info["banner"] as! String))
        
        self.navigationBar.setBackgroundImage(banner, forBarMetrics: .Default)
    }
    
}
