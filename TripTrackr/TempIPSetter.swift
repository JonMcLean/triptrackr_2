//
//  TempIPSetter.swift
//  TripTrackr
//
//  Created by Jon McLean on 21/06/2016.
//  Copyright © 2016 TripTrackr. All rights reserved.
//

import UIKit

class TempIPSetter: UIViewController {
    
    @IBOutlet var ipField: UITextField!
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        print("Temp IP View")
    }
    
    @IBAction func setIPField(sender: AnyObject) {
        urlBase = "http://\(self.ipField.text!):8080"
        print("URL Base")
        self.performSegueWithIdentifier("temp_load", sender: self)
    }
    
}
