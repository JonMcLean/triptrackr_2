//
//  Explore.swift
//  TripTrackr
//
//  Created by Jon McLean on 22/06/2016.
//  Copyright © 2016 TripTrackr. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import SwiftyJSON

class ExploreTabViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    @IBOutlet var map: GMSMapView!
    
    var manager: CLLocationManager = CLLocationManager()
    
    var latitude: CLLocationDegrees?
    var longitude: CLLocationDegrees?
    
    let storage = SessionStorage()
    
    var info: [String : AnyObject] = ["": ""]
    
    var markersData: [AnyObject] = []
    
    
    var tempPosStore: GMSCameraPosition?
    
    var posTimer = NSTimer()
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        info.removeAll()
        info = storage.loadSessionInfo()
        
        setupLocations()
        self.setupMapView()
        
        let visibleRegion = self.map.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(coordinate: visibleRegion.nearLeft, coordinate: visibleRegion.farRight)
        getData(bounds)
    }
    
    func setupLocations() {
        print("Setup")
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.startUpdatingLocation()
        
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .NotDetermined {
                print("not determined")
                manager.requestAlwaysAuthorization()
            }
        }else {
            let alertController = UIAlertController(title: "Location Services must be enabled", message: "For this app to work properly Location Services must be enabled on your device. To enable Location Services go into Settings > Privacy > Location Services", preferredStyle: .Alert)
            let okAlert = UIAlertAction(title: "Ok", style: .Default, handler: nil)
            
            alertController.addAction(okAlert)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse || CLLocationManager.authorizationStatus() == .AuthorizedAlways {
            print("Authorized")
            manager.startUpdatingLocation()
            //latitude = manager.location!.coordinate.latitude
            //longitude = manager.location!.coordinate.longitude
        }
        
    }
    
    func setupMapView() {
        let mapCamera = GMSCameraPosition.cameraWithLatitude(0, longitude: 0, zoom: 8.0)
        self.map.camera = mapCamera
        self.map.delegate = self
    }
    
    func getData(bounds: GMSCoordinateBounds) {
        let network = Networking()
        
        network.sendURLRequest(urlBase + "/api/explore/\(info["sessionCode"] as! String)/\(bounds.northEast.latitude)/\(bounds.southWest.longitude)/\(bounds.southWest.latitude)/\(bounds.northEast.longitude)") { (data, response, error) -> Void in
            if(error != nil) {
                
            }
            
            let code = (response as! NSHTTPURLResponse).statusCode
            
            if(code == 200) {
                
                let json = JSON(data: data!)
                self.markersData = json.arrayObject!
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    print("Filling")
                    self.fillMapMarkers()
                })
                
            }else {
                print("Code: \(code)")
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
    }
    
    func mapView(mapView: GMSMapView, didChangeCameraPosition position: GMSCameraPosition) {
        posTimer.invalidate()
        print("Changed GMSCameraPosition")
        posTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("runner:"), userInfo: ["position": position], repeats: false)
    }
    
    func runner(timer: NSTimer) {
        let position: GMSCameraPosition = timer.userInfo!["position"] as! GMSCameraPosition
        print("Run")
        /*let lLat: CLLocationDegrees = position.target.latitude
        let lLong: CLLocationDegrees = position.target.longitude*/
        
        let visibleRegion = self.map.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(coordinate: visibleRegion.nearLeft, coordinate: visibleRegion.farRight)
        
        self.getData(bounds)
    }
    
    func fillMapMarkers() {
        
        for data in markersData {
            
            print(data)
            
            let lLat: CLLocationDegrees = data["lat"] as! Double;
            let lLong: CLLocationDegrees = data["lng"] as! Double;
            let coord = CLLocationCoordinate2DMake(lLat, lLong)
            
            var marker = GMSMarker(position: coord)
            marker.userData = data
            marker.title = (data["name"] as! String)
            marker.map = self.map
            
        }
        
    }
    
    
    
    
}
